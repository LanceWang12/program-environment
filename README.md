# Program Environment

[toc]

## Normal Command

1. login_server

    ```bash
    # example
    login_server lance108
    ```

2. mount_server

    ```bash
    # example
    mount_server lance108
    ```

3. unmount_server

4. maintain

    ```bash
    # clean memory
    maintain
    ```

## Docker Command

1. Docker Commands are stored in ./WorkEnv
2. Some issue in mac
    - [operation not permitted in macos terminal](https://www.igorkromin.net/index.php/2020/11/18/how-to-fix-operation-not-permitted-error-in-macos-terminal-and-trash/)
        ![](https://i.imgur.com/Dbd8Dvm.png)



